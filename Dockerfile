ARG HOST

FROM exherbo/exherbo_did:${HOST}
ARG RUNNER_BUILD_JOBS

COPY build.sh /
RUN /build.sh ${RUNNER_BUILD_JOBS}
