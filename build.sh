#!/bin/bash

set -e

RUNNER_BUILD_JOBS=$1

eclectic env update
chgrp paludisbuild /dev/tty

export PALUDIS_DO_NOTHING_SANDBOXY=1

mkdir -p /etc/paludis/options.conf.d

sed -ri 's/jobs=[0-9]+/jobs='"${RUNNER_BUILD_JOBS:-5}"'/g' /etc/paludis/options.conf
echo '*/* build_options: -recommended_tests' >> /etc/paludis/options.conf.d/no-tests.conf

echo '*/* *' >> /etc/paludis/platforms.conf
echo 'sys-libs/libsyd perl python ruby' >> /etc/paludis/options.conf
echo 'dev-lang/python sqlite' >> /etc/paludis/options.conf

cave resolve -1x \
  repository/perl \
  repository/ruby \
  repository/rust \
  repository/mrothe

cave sync
cave generate-metadata

cave resolve -x -Ks \
  sys-libs/libseccomp \
  sys-libs/libsyd \
  dev-lang/rust \
  dev-perl/Math-Int64 \
  dev-util/gperf \
  net-apps/s3cmd \
  app-crypt/gnupg

# restore defaults
echo "Restoring default jobs"
sed -ri 's/jobs=[0-9]+/jobs=5/g' /etc/paludis/options.conf

echo "Cleaning up"
rm -f /build.sh
rm -f /etc/paludis/options.conf.d/no-tests.conf
rm -f /root/.bash_history
rm -Rf /tmp/*
rm -Rf /var/tmp/paludis/build/*
rm -Rf /var/cache/paludis/distfiles/*
rm -Rf /var/log/paludis/*
rm -f /var/log/paludis.log

